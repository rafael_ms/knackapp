#NOTES

#General
This app retrives data from knack relevant to Actions for Survival and generates a txt file to be imported by NZQA System.

#Node Modules - DO NOT IGNORE THIS FOLDER

Note that the node_modules are not being ignored in the gitignore file. DO NOT IGNORE THIS FOLDER
The reason for this, is that the knack module (knackhq-client - https://github.com/CamHenlin/knackhq-client) was modified in order to be able to get views from the API
A pull request was generated for this module, but it's author hasn't being active for over 1 year and the pull request wasn't commited or reviewed.
Possible solution, would be create a new npm module and submit it for approval.

On deploying to heroku, make sure you set phantomJS buildpack index to 1, otherwise the application won't work.
Use Command on your terminal -> heroku buildpacks:add --index 1 https://github.com/stomita/heroku-buildpack-phantomjs.git

#App Structure

##NZQA File

- GET Request -> routes/routes.js
- Controller gets called -> controller/index.js
- Tries to Connect with knack API -> controller/knackrequest.js
- Tries to Collect Data from System NZQA Units View -> controller/knackrequest.js
- Tries to Collect Data from System NZQA Ethnicities View -> controller/knackrequest.js
- Tries to Collect Data from System Learners View -> controller/knackrequest.js
- Proccess all Data and Generate Models -> controller/dataprocess.js
- Generate file and gives it back to the Controller -> controller/dataprocess.js
- Controller renders the file for download -> controller/index.js

###Certificates

- GET Request -> routes/routes.js
- Controller gets called -> controller/index.js
- Tries to Connect with knack API -> controller/knackrequest.js
- Tries to Collect Student Data from System -> controller/knackrequest.js
- Process Collected Data and Generate Certificate Model -> controller/dataprocess.js
- Renders page using certificate model data and returns html page into a string -> controller/render.js
- Creates temporary directory where pdf file can be generated -> controller/directory.js
- Tries to generate PDF file using phantomJS -> controller/phantom.js
- Render PDF file for download -> controller/render.js

## Templates
- NZQA TEMPLATE -> server/config/nzqa_template.json
- CERTIFICATE TEMPLATE -> server/config/certificate_template.json
- System Vars -> server/config/env