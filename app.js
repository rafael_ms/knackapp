var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var helmet = require('helmet');
var async = require('async');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, '/client/views'));
app.set('view engine', 'pug');

/*Async Load Middlewares*/

function parallel(middlewares) {
  return function (req, res, next) {
    async.each(middlewares, function (mw, cb) {
      mw(req, res, cb);
    }, next);
  };
}

app.use('/stylesheets',express.static(path.join(__dirname, 'client/public')));
app.use(favicon(path.join(__dirname, 'client/public', 'favicon.ico')));

app.use(parallel([
    logger('combined'),
    cookieParser(),
    express.static(path.join(__dirname, 'client/public')),
    helmet({frameguard: false }),
]));

app.use(errorHandler({ dumpExceptions: true, showStack: true }));


require('./server/routes/routes')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

module.exports = app;
