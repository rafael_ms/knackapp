module.exports = {
    db : {
	    user: '' //env var: PGUSER 
	  , database: '' //env var: PGDATABASE 
	  , password: '' //env var: PGPASSWORD 
	  , host: '' // Server hosting the postgres database 
	  , port:  0//env var: PGPORT 
	  , max:  0// max number of clients in the pool 
	  , idleTimeoutMillis:  0// how long a client is allowed to remain idle before being closed 
	},
	analytics: {
		script: ""
	},
	CDN : {
		path: ""
	},
	knack_data : {
		app : {
			apikey: "76076470-f309-11e6-a834-8f4745a32e4a"
			,appid: "58a24a4b67b18c00510a415c"
		},
		students: {
		 	scene : "scene_65"
			,view : "view_244"
		},
		afs_data : {
			scene: "scene_152"
			,view: "view_246"
		},
		nzqa_units : {
			scene : "scene_146"
			,view : "view_235"
		},
		certificates: {
			scene: "scene_161"
			,view: "view_260"
		},
		ethnicities: {
			scene: "scene_151"
			,view: "view_245"
		},
		general: {
			dataformat : "format=raw" // Output data to be pulled from server - Refeer to https://www.knack.com/developer-documentation/#formatting
			,filter: "field_35"
			,rows_per_page: 1000
		}
	}
};