var models = require('../models/models');
var utils = require('../misc/utils');
var template = require('../config/nzqa_template.json');

exports.processData = function(data){
	utils.log('debug', 'Controller', JSON.stringify('Started'), 'processData');

	//data is an array
	//data - Student information

	return new Promise(function(resolve,reject){
		try{

			//Loop through Student array, new Learner object
			//Remember to use "toString()" so that you can contact all students into a single variable

			//data is an array containing
			//data[0] - array with NZQA Units [0] and Ethnicities[1]
			//data[1] - array of objects with Learners

			var units = nzqaUnitsModel(data[0][0]);

			var ethnicities = ethnicitiesModel(data[0][1]);

			var students = learnerModel(
											 units
											,ethnicities
											,data[1]
										);

			var result = new models.NZQA(
				 new models.Header().toString()
				,students
			).toString();

			utils.log('debug', 'Controller', JSON.stringify('Data Process Done'), 'processData');

			//Remove this to go live on heroku
			if(process.env.NODE_ENV == 'production')
				resolve('Nothing here ;)');
			else
				resolve(result);
		}
		catch(err){
			reject(err);
		}

	});
}

function nzqaUnitsModel(nzqaUnits){
	var units = [];

	utils.log('debug', 'Model', JSON.stringify('Start'), 'nzqaUnitsModel');

	nzqaUnits.forEach(function(item,key){
		units.push(new models.NZQAunits( item[template.nzqa_units[0].afsCourse]									//afsCourse, don't normalize this is an array
										,utils.normalizeItem(item[template.nzqa_units[0].nzqaCourse])			//nzqaCourse
										,utils.normalizeItem(item[template.nzqa_units[0].code])					//code
										,utils.normalizeItem(item[template.nzqa_units[0].credent])				//credent
										,utils.normalizeItem(item[template.nzqa_units[0].level])				//level
										,utils.normalizeItem(item[template.nzqa_units[0].version])				//version
										));
	});

	utils.log('debug', 'Model', JSON.stringify('Finish'), 'nzqaUnitsModel');

	return units;
}

function ethnicitiesModel(ethnicities){
	var ethn = [];

	utils.log('debug', 'Model', JSON.stringify('Start'), 'ethnicitiesModel');

	ethnicities.forEach(function(item,key){
		ethn.push(new models.Ethnicities( utils.normalizeItem(item[template.ethnicities[0].descrition])			//descrition
										  ,utils.normalizeItem(item[template.ethnicities[0].code])				//code
										));
	});

	utils.log('debug', 'Model', JSON.stringify('Finish'), 'ethnicitiesModel');

	return ethn;
}

function learnerModel(units, ethnicitiesArray, learners){
	var students = '';

	utils.log('debug', 'Model', JSON.stringify('learnerModels Start'), 'learnerModels');

	learners.forEach(function(item,key){

		var ethnicities = getEthnicities(ethnicitiesArray, item[template.learner[0].ethnic.knack_field]);


		students += new models.Learner(
			  utils.normalizeItem(item[template.learner[0].local_id.knack_field])								//local_id - Student ID in Student View
			, utils.normalizeItem(item[template.learner[0].nsn.knack_field])									//nsn
			, utils.normalizeItem(item[template.learner[0].last_name.knack_field])								//last_name
			, utils.normalizeItem(item[template.learner[0].first_name.knack_field])								//first_name
			, utils.normalizeItem(item[template.learner[0].previous_name.knack_field])							//previous_name
			, utils.normalizeItem(item[template.learner[0].address.knack_field])								//address
			, utils.normalizeItem(item[template.learner[0].dob.knack_field]['date'])							//dob
			, utils.normalizeItem(item[template.learner[0].gender.knack_field])									//gender
			, ethnicities																						//ethnic - 3 at most, 3 char each - 9 char max
		).toString();

		utils.log('debug', 'Controller', JSON.stringify('Learner Model Done'), 'processData');

		if(utils.isArray(item[template.result[0].knack_result_field])){

			utils.log('debug', 'Controller', JSON.stringify('Result Model Array Start'), 'processData');

			var course = utils.normalizeItem(item[template.learner[0].qualification.knack_field]);
			var nzqa_units = utils.findObject( units
											,template.nzqa_units[0].model_prop
											,course);

			item[template.result[0].knack_result_field].forEach(function(result,key){

				students += new models.Result(
					 utils.normalizeItem(item[template.result[0].local_id.knack_field])							//local_id - Student ID in Student View
					,utils.normalizeItem(item[template.result[0].date_completed.knack_field][0])				//date_completed - assessment date
					,nzqa_units[key].code																		//Standard Code
					,nzqa_units[key].version																	//Version
					,utils.normalizeItem(item[template.result[0].result.knack_field])							//result -
					).toString();
			});

			utils.log('debug', 'Controller', JSON.stringify('Result Model Array Done'), 'processData');
		}
		else{
			utils.log('debug', 'Controller', JSON.stringify('Result Model Single Start'), 'processData');

			var course = utils.normalizeItem(item[template.learner[0].qualification.knack_field]);
			var nzqa_units = utils.findObject( units
										,template.nzqa_units[0].model_prop
										,course);

			students += new models.Result(
					 utils.normalizeItem(item[template.result[0].local_id.knack_field])							//local_id - Student ID in Student View
					,utils.normalizeItem(item[template.result[0].date_completed.knack_field][0])				//date_completed - assessment date
					,nzqa_units.code																			//Standard Code
					,nzqa_units.version																			//Version
					,utils.normalizeItem(item[template.result[0].result.knack_field])							//result -
			).toString();

			utils.log('debug', 'Controller', JSON.stringify('Result Model Single Done'), 'processData');
		}
	});

	utils.log('debug', 'Model', JSON.stringify('learnerModels Done'), 'learnerModels');

	return students;
}

function getEthnicities(ethnicities, ethnItem){
	var result = '';
	//More than one ethnicity
	if(utils.isArray(ethnItem)){
		ethnItem.forEach(function(ethn,key){
			utils.log('debug', 'Controller', JSON.stringify('Multi Ethnicity Start'), 'processData');
			result += utils.findObject( ethnicities
										,template.ethnicities[0].model_prop
										,utils.normalizeItem(ethn))[0].code ;
		});
	}
	else{
		utils.log('debug', 'Controller', JSON.stringify('Single Ethnicity Start'), 'processData');
		result = utils.findObject( ethnicities
						,template.ethnicities[0].model_prop
						,utils.normalizeItem(ethnItem))[0].code ;
	}

	return result;
}

exports.processCertificateData = function(data){
	utils.log('debug', 'Controller', JSON.stringify('Started'), 'processCertificateData');

	return new Promise(function(resolve,reject){
		try{
			var certificateData = certificateModel(data);

			resolve(certificateData);
		}
		catch(err){
			reject(err);
		}
	})
}

function certificateModel(studentData){
	utils.log('debug', 'Model', JSON.stringify('Start'), 'certificateModel');

	utils.log('debug', 'Model', JSON.stringify(studentData), 'certificateModel');

	var result = new models.Certificate( utils.normalizeItem(studentData[template.certificate[0].student_firstname])
									,utils.normalizeItem(studentData[template.certificate[0].student_lastname])
									,utils.normalizeItem(studentData[template.certificate[0].course_status])
									,utils.normalizeItem(studentData[template.certificate[0].course])
									,utils.normalizeItem(studentData[template.certificate[0].date])
									,utils.normalizeItem(studentData[template.certificate[0].trainer])
									,utils.normalizeItem(studentData[template.certificate[0].units]));

	utils.log('debug', 'Model', JSON.stringify('Finish'), 'certificateModel');

	return result;
}