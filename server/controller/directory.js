var utils = require('../misc/utils');

exports.getTempDirectory = function(obj){
	return new Promise(function(resolve, reject){
		var temp = require('tmp');

		var tmpobj = temp.dirSync();

		obj.push(tmpobj);

		utils.log('debug', 'controller', JSON.stringify('Create Temp Dir Complete'), 'tempDir');

		resolve(obj);
	});
}