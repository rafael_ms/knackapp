var Promise = require('promise');
var knack = require('./knackrequest');
var dataprocess = require('./dataprocess');
var render = require('./render');
var phantom = require('./phantom');
var directory = require('./directory');
var utils = require('../misc/utils');



exports.home = function(request, response){
	//Promises sequence
	//1 - Connect to knack app
	//2 - Collect AFS data
	//3 - Collect Students data
	//4 - Process data
	//5 - Render data (Rendering view for tests, should respond with file for download)

	//Can't filter view
	//var filterDate = true;
	//if(Object.keys(request.query).length > 0)
	//	filterDate = utils.paramValidate(request.param('date')); //US Date MM/DD/YYYY

	knack.knackConnect()
	.then(knack.knackCollectNZQAUnits)
	.then(knack.knackCollectEthnicities)
	.then(knack.knackCollect)
	.then(dataprocess.processData)
	.then(function(result){
		render.File(
			 result
			,response
		);
	})
	.catch(function(error){
		utils.log('debug', 'controller', JSON.stringify(error), 'Home Promise Catch');
	});

};

exports.certificate = function(request, response){

	var id = request.params.id;

	knack.knackConnect()
	.then(knack.knackCollectSingleObject.bind(null,id))
	.then(dataprocess.processCertificateData) // check for catches on model error
	.then(function(result){
		return new Promise(function(resolve, reject){
			if(result.course_status != 'c'){
				utils.log('debug', 'controller', JSON.stringify('error'), 'Home Promise Catch');
				render.NotCompleted(
					'Not Completed'
					,response
				);
			}
			else{
				var renderResult = render.toVariable([response,request,result]); //Try to improve this function and bind the variables to a single Promise Call instead of Promise neasted function.
				resolve(renderResult);
			}			
		});
	})
	.then(directory.getTempDirectory) //Improve code for deleting folder on any catch
	.then(phantom.renderPDF)
	.then(function(result){
		render.PDF(
			result
			,response
		);
	})
	.catch(function(error){
		utils.log('debug', 'controller', JSON.stringify(error), 'Home Promise Catch');
	});
};