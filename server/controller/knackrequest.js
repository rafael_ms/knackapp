var Promise = require('promise');
var knack =  require('knackhq-client');
var utils = require('../misc/utils');
var config = require('../config/config');

exports.knackConnect = function(){
	return new Promise(function(resolve, reject){

		knack.authenticate({
			 apikey: config.knack_data.app.apikey
			,appid: config.knack_data.app.appid
		}, function(error){
			if (error) {
		    	// failed to log in!
		    	reject(error);
		    	utils.log('debug', 'controller', 'Login Fail!', 'Knack Connect');
		  	}

		  	utils.log('debug', 'controller', JSON.stringify('Connected'), 'knackConnect');

		  	resolve('connected');
		});
	});
}

//Get All Learners
exports.knackCollect = function(results){
	return new Promise((resolve, reject) => {

		//can't filter view
		//var filters = '';
		//if(!filterDate)
		//	reject('Invalid Parameter');
		//else if(filterDate.length > 0)
		//	filters = { filters: [{ field: config.knack_data.general.filter , operator: 'is', value: filterDate }] };
		//utils.log('debug', 'controller', JSON.stringify(filters), 'knackCollect');

		knack.view_records({
			  scene: config.knack_data.students.scene
			, view: config.knack_data.students.view
			, format: config.knack_data.general.dataformat
			, rows_per_page: config.knack_data.general.rows_per_page
		}, function(error, students) {
			if (error) {
			// something bad happened!
				utils.log('debug', 'controller', JSON.stringify(error), 'knackCollect - Error');
				reject(error);
			}

			if (!students.records || !students.records.length) {
				// no records
				utils.log('debug', 'controller', JSON.stringify(students), 'knackCollect - No data');
				resolve('No Data');
			}
				utils.log('debug', 'controller', JSON.stringify('Collect Learners Successful'), 'knackCollect');
				//Only returns records
				//Use afs for company related info
				resolve( [ results , students['records'] ] );
			});
		});
}

//Get All NZQA Units set by the user
exports.knackCollectNZQAUnits = function(){
	return new Promise((resolve, reject) => {

		utils.log('debug', 'controller', JSON.stringify('entered'), 'knackCollectNZQAUnits');

		knack.view_records({
			  scene: config.knack_data.nzqa_units.scene
			, view: config.knack_data.nzqa_units.view
			, format: "both" //config.knack_data.general.dataformat
			, rows_per_page: config.knack_data.general.rows_per_page
		}, function(error, nzqaUnits) {
			if (error) {
			// something bad happened!
				utils.log('debug', 'controller', JSON.stringify(error), 'knackCollectNZQAUnits - Error');
				reject(error);
			}

			if (!nzqaUnits.records || !nzqaUnits.records.length) {
				// no records
				utils.log('debug', 'controller', JSON.stringify('No Data'), 'knackCollectNZQAUnits');
				resolve('No Data');
			}
				utils.log('debug', 'controller', JSON.stringify('Collect NZQA Units Successful'), 'knackCollectNZQAUnits');

				//Only returns records
				//utils.log('debug', 'controller', JSON.stringify(nzqaUnits), 'KN');
				resolve(nzqaUnits['records']);
			});
		});
}

//Get All Ethnicities set by the user
exports.knackCollectEthnicities = function(nzqaUnits){
	return new Promise((resolve, reject) => {

		utils.log('debug', 'controller', JSON.stringify('entered'), 'knackCollectEthnicities');

		knack.view_records({
			  scene: config.knack_data.ethnicities.scene
			, view: config.knack_data.ethnicities.view
			, format: config.knack_data.general.dataformat
			, rows_per_page: config.knack_data.general.rows_per_page
		}, function(error, ethnicities) {
			if (error) {
			// something bad happened!
				utils.log('debug', 'controller', JSON.stringify(error), 'knackCollectEthnicities - Error');
				reject(error);
			}

			if (!ethnicities.records || !ethnicities.records.length) {
				// no records
				utils.log('debug', 'controller', JSON.stringify('No Data'), 'knackCollectEthnicities');
				resolve('No Data');
			}
				utils.log('debug', 'controller', JSON.stringify('Collect Ethnicities Successful'), 'knackCollectEthnicities');

				//Only returns records
				resolve([ nzqaUnits, ethnicities['records'] ]);
			});
		});
}

//Get Single Object from view
exports.knackCollectSingleObject = function(object_id){
	return new Promise((resolve, reject) => {
		utils.log('debug', 'controller', JSON.stringify('entered'), 'knackCollectSingleObject');

		knack.view_single_object({
			  scene: config.knack_data.certificates.scene
			, view: config.knack_data.certificates.view
			, object_id: object_id
			, format: config.knack_data.general.dataformat
		}, function(error, singleObject) {
			if (error) {
			// something bad happened!
				utils.log('debug', 'controller', JSON.stringify(error), 'knackCollectSingleObject - Error');
				reject(error);
			}

			if (!singleObject || singleObject.length == 0) {
				// no records
				utils.log('debug', 'controller', JSON.stringify('No Data'), 'knackCollectSingleObject');
				resolve('No Data');
			}
				utils.log('debug', 'controller', JSON.stringify('Collect Single Object Successful'), 'knackCollectSingleObject');

				//Only returns records
				resolve(singleObject);
		});
	})
}