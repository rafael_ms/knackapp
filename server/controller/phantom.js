var utils = require('../misc/utils');

exports.renderPDF = function(obj){
	return new Promise(function(resolve, reject){
		var phantom = require('phantom');

		phantom.create(['--ignore-ssl-errors=yes']).then(ph => {
		    _ph = ph;
		    return _ph.createPage();
		}).then(page => {
		    _page = page;
		    return _page.property('onConsoleMessage', function (msg) {
		        utils.log('debug', 'controller',msg);
		    });
		}).then(() => {
		    utils.log('debug', 'controller','Setting paperSize', 'Phantom renderPDF');
		    return _page.property('paperSize', {format: 'A4', orientation: 'portrait'})
		}).then(() => {
		    utils.log('debug', 'controller','paperSize set successfully', 'Phantom renderPDF');
		    utils.log('debug', 'controller','Setting viewportSize', 'Phantom renderPDF');
		    return _page.property('viewportSize', {width: 1920, height: 1080});
		}).then(() => {
		    utils.log('debug', 'controller','viewportSize set successfully', 'Phantom renderPDF');
		    utils.log('debug', 'controller','Setting DPI', 'Phantom renderPDF');
		    return _page.property('dpi', 300)
		}).then(() => {
		    utils.log('debug', 'controller','DPI set successfully', 'Phantom renderPDF');
		    utils.log('debug', 'controller','Opening report', 'Phantom renderPDF');
		    return _page.setContent(obj[1], obj[0].protocol + '://' + obj[0].get('host') + obj[0].originalUrl);
		}).then(status => {
		    utils.log('debug', 'controller',status, 'Phantom renderPDF');
		    utils.log('debug', 'controller','Preparing to render document', 'Phantom renderPDF');

		    _page.render(obj[2].name + '/google.pdf');
		}).then(() => {
		    utils.log('debug', 'controller','Rendering completed', 'Phantom renderPDF');
		    utils.log('debug', 'controller','Phantom process completed successfully', 'Phantom renderPDF');
		    _page.close();
		    _ph.exit();
		}).then(() => {
			obj.push('/google.pdf');
			obj.shift();
			resolve(obj);
		}).catch(function(error){
			utils.log('debug', 'controller', JSON.stringify(error), 'Phantom renderPDF Catch');
			utils.log('debug', 'controller',error, 'Phantom renderPDF Catch');
		});
	});
}