var utils = require('../misc/utils');

exports.File = function(data,response){

	response.set({"Content-Disposition":"attachment; filename=text.txt"});
	response.setHeader('Content-type', 'text/plain');
	response.charset = 'UTF-8';
	response.send(data);

	response.end();
}

exports.NotCompleted = function(data,response){

	response.set({"Content-Disposition":"attachment; filename=course.notcompleted"});
	response.setHeader('Content-type', 'text/plain');
	response.charset = 'UTF-8';
	response.send(data);

	response.end();
}

exports.Page = function(data,response){

	response.render('test', {
		    title: 'Actions For Survival',
		    data: data
	});

	response.end();
}

exports.toVariable = function(obj){
			var moment = require('moment');
			var page = "";

			obj[0].render('index', {
				"student_firstname": obj[2].student_firstname
				,"student_lastname": obj[2].student_lastname
				,"course" : obj[2].course.replace('no units','')
				,"trainer" : obj[2].trainer
				,"units" : obj[2].nzqa_units
				,"date" : moment(obj[2].date,'DD/MM/YYYY').format('Do MMMM YYYY')
				,"director" : obj[2].director
				,'certificate_template' : obj[2].certificate_template
			},function(err, html) {
			    page = html;
			});
			utils.log('debug', 'controller', JSON.stringify('Render Cert Page Complete'), 'renderPage');
			obj.shift();
			obj.pop();
			obj.push(page);
			return obj;
}


exports.PDF = function(data,response){

	var fs = require('fs'),
		waitUntil = require('wait-until');

	var file = data[1].name + '/' + data[2];

	waitUntil()
	.interval(1000)
	.times(10)
	.condition(function condition() {
	    return (fs.existsSync(file) ? true : false);
	})
	.done(function done(result) {
	    // result is true on success or false if the condition was never met 
	    if(result){
	    	response.download(file, function(err){
	    		if (err) {
				    // Handle error, but keep in mind the response may be partially-sent
				    // so check res.headersSent
				    console.log(err);
				 } else {
		    		fs.unlinkSync(data[1].name + '/' + data[2]);
					data[1].removeCallback();
				}
	    	});
	    }
	});

}