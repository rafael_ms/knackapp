const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/exportdb';
var config = require('../config/config');

exports.connect = function(res, sql, params){
  const results = [];
  pg.defaults.ssl = false;
  pg.connect(config.db, (err, client, done) => {
      // Handle connection errors
      if(err) {
        done();
        console.log(err);
        return res.status(500).json({success: false, data: err});
      }
      // SQL Query > Insert Data
      const query = client.query(sql, params);
      // SQL Query > Select Data

      // After all data is returned, close connection and return results
      query.on('end', () => {
        done();
      });
  });
}

exports.normalizeItem = function(item){
  if(isObject(item)){
    if(!isUndefined(item['identifier']))
      item = item['identifier'];
    else
      item = emptyItem();
  }
  if(isArray(item)){
    if(item.length > 0){
      if(item.length > 1){
        var result = [];
        item.forEach(function(itemArr,key){
          result.push(getNormalizedItem(item, key));
        });

        item = result.join(", ");
      }
      else
        item = getNormalizedItem(item);
    }
    else
      item = emptyItem();
  }
  if(item == null || isUndefined(item))
    item = emptyItem();

  return item;
}

function getNormalizedItem(item,key){

  key = key || 0;

  if(!isUndefined(item[key]['identifier']))
    return item[key]['identifier'];
  else
    return emptyItem();
}

exports.paramValidate = function(date){

  if(isNaN(Date.parse(date)))
    return false;

  return date;
}

exports.fillWith = function(filling,length){
  return new Array( length + 1 ).join(filling);
}

exports.getDate = function(date){
  var today = new Date();

  if(date == 'time')
    return getHour(today);

  if(date != null & (date != 'time' && date != 'year' && date != 'formatted'))
    today = new Date(date);

  var dd = today.getDate().toString();
  var mm = (today.getMonth() +1 ).toString(); //January is 0!
  var yyyy = today.getFullYear().toString();

  if(date == 'year')
    return yyyy;

  if(dd<10)
      dd='0' + dd;
  if(mm<10)
      mm='0' + mm;

  if(date == 'formatted')
    return dd + '/' + mm + '/' + yyyy;
  else
    return yyyy + mm + dd ;
}

function getHour(date){
  var hour = date.toTimeString().split(' ')[0];
  hour = hour.split(/\:|\-/g);
  return hour[0] + ':' + hour[1];
}

function isObject(item){
  if(Object.prototype.toString.call(item) === '[object Object]')
    return true;

  return false;
}

exports.isArray = function(item){
  if(Object.prototype.toString.call(item) === '[object Array]')
    return true;

  return false;
}

function isArray(item){
  if(Object.prototype.toString.call(item) === '[object Array]')
    return true;

  return false;
}

function isUndefined(item){
  if(typeof item  === 'undefined')
    return true;

  return false;
}

exports.findObject = function(arr, attr ,param){
  var result = [];
  arr.find(function(item,key){
    if(isArray(item[attr])){
      var added = false;
      item[attr].forEach(function(itemArr,key){
          if(itemArr == param)
            if(!added){
              result.push(item);
              added = true;
            }
      });
    }
    else if(item[attr] ==  param)
      result.push(item);
  });

  if(isUndefined(result) || result.length == 0)
    return {"code" : "", "level" : "", "version" : "" };

  return result;
}

function nameAbbreviator(name) {
  if (name.length > 25) {
    var arr = name.split(' ');
    var newarr = [];
    arr.forEach(function(item, key,arrr) {
      if (key != 0 && key != (arr.length - 1)) {
        if (item.length > 2)
          item = item[0] + '.';
      }

      newarr.push(item);
    });

    var newname = fullName(newarr);

    while(newname.length > 25){
      newarr.splice((newarr.length - 2), 1);
      newname = fullName(newarr);
    }

    return newname;
  }
}

function fullName(nameArr){
  return nameArr.join(' ');
}

function emptyItem(){
  return "";
}

exports.emptyModel = function(){
  return "";
}

exports.log = function(severity, key, val, text) {
    text = text || '';

    if (typeof (console[severity]) === 'function') {
      console[severity]('[log]  \x1B[90m%s:\x1B[0m \x1B[36m%s %s\x1B[0m', key, val, text);
    } else {
      console.error('[log]  \x1B[90m%s:\x1B[0m \x1B[36m%s %s\x1B[0m', key, val, text);
    }
};