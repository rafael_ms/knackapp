const template = require('../config/nzqa_template.json');
const certificates_template = require('../config/certificates_template.json');
var utils = require('../misc/utils');

exports.Header = function(){

	this.record_type = setItem( utils.emptyModel()
		, template.header[0].record_type.length
		, template.header[0].record_type.default_value
		);
	this.file_type = setItem( utils.emptyModel()
		, template.header[0].file_type.length
		, template.header[0].file_type.default_value
		);
	this.reporting_provider_code = setItem( utils.emptyModel()
		, template.header[0].reporting_provider_code
		, template.header[0].reporting_provider_code.default_value
		);
	this.date = setItem( utils.getDate('formatted')
		, template.header[0].date.length
		, template.header[0].date.default_value
		);
	this.time = setItem( utils.getDate('time')
		, template.header[0].time.length
		, template.header[0].time.default_value
		);
	this.software_id = setItem( utils.emptyModel()
		, template.header[0].software_id.length
		, template.header[0].software_id.default_value
		);
	this.reporting_provider_location = setItem( 'reporting_provider_location'
		, template.header[0].reporting_provider_location.length
		, template.header[0].reporting_provider_location.default_value
		);
	this.year = setItem( utils.getDate('year')
		, template.header[0].year.length
		, template.header[0].year.default_value
		);
	this.provider_reference = setItem( providerRef()
		, template.header[0].provider_reference.length
		, template.header[0].provider_reference.default_value
		);
	this.awarding_provider_code = setItem( utils.emptyModel()
		, template.header[0].awarding_provider_code.length
		, template.header[0].awarding_provider_code.default_value
		);
	this.awarding_provider_location = setItem( utils.emptyModel()
		, template.header[0].awarding_provider_location.length
		, template.header[0].awarding_provider_location.default_value
		);

	this.toString = function(){
		return "".concat(
			this.record_type
			,this.file_type
			,this.reporting_provider_code
			,this.date
			,this.time
			,this.software_id
			,this.reporting_provider_location
			,this.year
			,this.provider_reference
			,this.awarding_provider_code
			,this.awarding_provider_location
			,newLine()
			);
	}

}

exports.Learner = function(
	local_id
	,nsn
	,last_name
	,first_name
	,previous_name
	,address
	,dob
	,gender
	,ethnic ){

	this.record_type = setItem( utils.emptyModel()
		, template.learner[0].record_type.length
		, template.learner[0].record_type.default_value
		);
	this.local_id = setItem( local_id
		, template.learner[0].local_id.length
		, template.learner[0].local_id.default_value
		);
	this.nsn = setItem( nsn
		, template.learner[0].nsn.length
		, template.learner[0].nsn.default_value
		);
	this.hook_flag = setItem( utils.emptyModel()
		, template.learner[0].hook_flag.length
		, template.learner[0].hook_flag.default_value
		);
	this.last_name = setItem( last_name
		, template.learner[0].last_name.length
		, template.learner[0].last_name.default_value
		);
	this.first_name = setItem( first_name
		, template.learner[0].first_name.length
		, template.learner[0].first_name.default_value
		);
	this.previous_name = setItem( previous_name
		, template.learner[0].previous_name.length
		, template.learner[0].previous_name.default_value
		);
	this.address = setItem( address
		, template.learner[0].address.length
		, template.learner[0].address.default_value
		);
	this.dob = setItem( utils.getDate(dob)
		, template.learner[0].dob.length
		, template.learner[0].dob.default_value
		);
	this.gender = setItem( gender
		, template.learner[0].gender.length
		, template.learner[0].gender.default_value
		);
	this.ethnic = setItem( ethnic
		, template.learner[0].ethnic.length
		, template.learner[0].ethnic.default_value
		);
	this.qualification = setItem( utils.emptyModel()
		, template.learner[0].qualification.length
		, template.learner[0].qualification.default_value
		);

	this.toString = function(){
		return "".concat(
			this.record_type
			,this.local_id
			,this.nsn
			,this.hook_flag
			,this.last_name
			,this.first_name
			,this.previous_name
			,this.address
			,this.dob
			,this.gender
			,this.ethnic
			,this.qualification
			,newLine()
			);
	}
}

exports.Result = function(
	 local_id
	,date_completed
	,standard_code
	,version
	,result	){

	this.record_type = setItem(   utils.emptyModel()
		, template.result[0].record_type.length
		, template.result[0].record_type.default_value
		);

	this.local_id = setItem(  local_id
		, template.result[0].local_id.length
		, template.result[0].local_id.default_value
		);

	this.standard_code = setItem( standard_code
		, template.result[0].standard_code.length
		, template.result[0].standard_code.default_value
		);

	this.version = setItem(   version
		, template.result[0].version.length
		, template.result[0].version.default_value
		);

	this.language = setItem(  utils.emptyModel()
		, template.result[0].language.length
		, template.result[0].language.default_value
		);

	this.date_completed = setItem( utils.getDate(date_completed)
		, template.result[0].date_completed.length
		, template.result[0].date_completed.default_value
		);

	this.result = setItem(    result
		, template.result[0].result.length
		, template.result[0].result.default_value
		);

	this.toString = function(){
		return "".concat(
			this.record_type
			,this.local_id
			,this.standard_code
			,this.version
			,this.language
			,this.date_completed
			,this.result
			,newLine()
			);
	}
}

exports.NZQAunits = function(
		 afsCourse
		,nzqaCourse
		,code
		,credent
		,level
		,version
	){

	this.afsCourse = afsCourse ;
	this.nzqaCourse = nzqaCourse ;
	this.code = code ;
	this.credent = credent ;
	this.level = level ;
	this.version = version ;
}

exports.NZQAunitsTest = function(
		 afsCourse
		,code
		,unit_code
	){

	this.afsCourse = afsCourse ;
	this.code = code ;
	this.unit_code = unit_code ;
}

exports.Ethnicities = function(
		description
		,code
	){
	this.description = description;
	this.code = code;
}

exports.NZQA = function(
	header
	,learner ){

	//Model for NZQA File Template
	//Include all variables

	this.header = header;
	this.learner = learner;

	this.toString = function(){
		return "".concat(
				 header
				,learner
			);
	}

}

exports.Certificate = function(
	 student_firstname
	,student_lastname
	,course_status
	,course
	,date
	,trainer
	,nzqa_units) {

	this.student_firstname = student_firstname;
	this.student_lastname = student_lastname;
	this.course_status = course_status.toLowerCase();
	this.course = course;
	this.date = date;
	this.trainer = trainer;
	this.nzqa_units = nzqa_units.length > 0 ? "(" + nzqa_units + ")" : utils.emptyModel();
	this.director = certificates_template.default.director
	this.certificate_template = getTemplate(course);
}

function getTemplate(course){
	var result = certificates_template.courses.filter(function(item,key){
		return item.name == course;
	});

	return result.length == 0 ? certificates_template.default.template : result[0].template;
}

function setItem(
	  item
	, length
	, default_value ){

	item = item.toString();

	if(default_value.length > 0)
		item = default_value;

	if(item.length < length){
		var add = length - item.length;
		item = item + utils.fillWith(' ', add);
	}
	else if(item.length > length)
		item = item.substring(0,length);

	return item.toUpperCase();
}


function providerRef(){
	var date = new Date();
	return 'AFS' + utils.getDate(null);
}


function newLine(){
	return "\r\n";
}