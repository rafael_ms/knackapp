var index = require('../controller/index');

module.exports = function(app){

	/* GET home page. */
	app.get('/', index.home);

	/* GET Certificate */
	app.get('/certificate/:id', index.certificate);
}